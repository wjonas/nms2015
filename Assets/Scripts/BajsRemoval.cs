﻿using UnityEngine;
using System.Collections;

public class BajsRemoval : MonoBehaviour {

	SpriteRenderer	_renderer;
	private float lifeTime = 0.0f;
	public float life = 10.0f;
	public float fadeTime = 1.0f;

	// Use this for initialization
	void Start () {
		_renderer = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
		lifeTime += Time.deltaTime;
		
		if( lifeTime > life )
		{
			Destroy (gameObject);
		}
		
		
		float startFade = life - fadeTime;
		
		float opacity = 1.0f;
		
		if( lifeTime > startFade )
		{
			float f = lifeTime - startFade;
			float norm = f / fadeTime;
			opacity = 1.0f - norm;
		}
		
		Color c = _renderer.color;
		Color n = new Color( c.r, c.g, c.g, opacity );
		_renderer.color = n;
	}
}
