﻿using UnityEngine;
using System.Collections;

public class DestroyOnFinishParticles : MonoBehaviour {

	ParticleSystem _particles;

	// Use this for initialization
	void Start () {
		_particles = GetComponent<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update () {
		if( ! _particles.IsAlive() )
		{
			Destroy ( gameObject );
		}
	}
}
