﻿using UnityEngine;
using System.Collections;

public class TrainingRoomInput : MonoBehaviour
{
	public AudioClip okClickSFX;
	public AudioClip errorClickSFX;
	private AudioSource audioSource;

	public float switchTrainingModeCooldown = 1f;
	private float lastSwitchTrainingMode;

	public Color playerColor = Color.white;

	private TrainingRoom trainingRoom;
	
	private Gamepad _gamepad;

	public Gamepad GetGamepad()
	{
		return _gamepad;
	}

	private void Start()
	{
		audioSource = GetComponent<AudioSource> ();
		trainingRoom = GetComponent<TrainingRoom> ();

		_gamepad = GetComponent<Gamepad>();
	}

	private void Update()
	{
		// A BUTTON
		if(_gamepad.IsButtonDown(Gamepad.A_BUTTON))
		{
			if(trainingRoom.TrainStat())
			{
				// TODO: Play training sounds from the training room
			}
			else if(errorClickSFX != null)
			{
				audioSource.PlayOneShot(errorClickSFX);
			}
		}
		
		
		
		
		// B BUTTON
		if(_gamepad.IsButtonDown(Gamepad.B_BUTTON))
		{
			//Debug.Log ("ARENA");
			trainingRoom.OpenDoor((tr) => tr.MoveMonsterToArena(), null);
			//trainingRoom.ReleaseAllMonsters();
		}

		// X BUTTON
		if(_gamepad.IsButtonDown(Gamepad.X_BUTTON))
		{

		}

		// Y BUTTON
		if(_gamepad.IsButtonDown(Gamepad.Y_BUTTON))
		{

		}

		// START BUTTON
		if(_gamepad.IsButtonDown(Gamepad.START_BUTTON))
		{
			
		}
		
		// BACK BUTTON
		if(_gamepad.IsButtonDown(Gamepad.BACK_BUTTON))
		{
			
		}
		
		// D-PAD HORIZONTAL AXIS
		float leftRight = _gamepad.UpdateAxis(Gamepad.DPAD_HORIZONTAL_AXIS);
		if(leftRight < -0.8f)
		{
			if(Time.time - lastSwitchTrainingMode >= switchTrainingModeCooldown)
			{
				PlayClickSFX();
				lastSwitchTrainingMode = Time.time;
				trainingRoom.PreviousTrainingMode ();
			}
		}
		else if(leftRight > 0.8f)
		{
			if(Time.time - lastSwitchTrainingMode >= switchTrainingModeCooldown)
			{
				PlayClickSFX();
				lastSwitchTrainingMode = Time.time;
				trainingRoom.NextTrainingMode ();
			}
		}
		
		// D-PAD VERTICAL AXIS
		float upDown = _gamepad.UpdateAxis(Gamepad.DPAD_VERTICAL_AXIS);

		// left stick
		//float leftXAxis = UpdateAxis (L_X_AXIS);
		//float leftYAxis = UpdateAxis (L_Y_AXIS);

		CheatButtons ();
	}

	private void PlayClickSFX()
	{
		if(trainingRoom.HasMonster)
		{
			audioSource.PlayOneShot(okClickSFX);
		}
		else
		{
			audioSource.PlayOneShot(errorClickSFX);
		}
	}

	private void CheatButtons ()
	{
		if (Input.GetKeyUp (KeyCode.Alpha1)) {
			trainingRoom.ReleaseAllMonsters();
		}

		if (Input.GetKeyUp (KeyCode.Alpha2)) {
			foreach(TrainingRoom tr in GameObject.FindObjectsOfType<TrainingRoom>())
			{
				tr.CheatTrainSwoosh();
			}
		}

		if (Input.GetKeyUp (KeyCode.Alpha3)) {
			foreach(TrainingRoom tr in GameObject.FindObjectsOfType<TrainingRoom>())
			{
				tr.CheatTrainStrongth();
			}
		}

		if (Input.GetKeyUp (KeyCode.Alpha4)) {
			foreach(TrainingRoom tr in GameObject.FindObjectsOfType<TrainingRoom>())
			{
				tr.CheatTrainScience();
			}
		}
	}
}