﻿using UnityEngine;
using System.Collections;
using System;
using Ebbe.Cereal;
using UnityEngine.UI;
using XInputDotNetPure;

public class TrainingRoom : MonoBehaviour
{
	const float VIBRATION_POWER = 0.5f;

	const float TRAINING_PER_CLICK = 0.1f;
	const float MAX_STAT_VALUE = 10f;

	public enum TrainingMode
	{
		NONE,
		SPEED,
		FIGHTING,
		SCIENCE
	}
	public TrainingMode currentTrainingMode = TrainingMode.SPEED;
	public Transform speedTraining;
	public Transform fightingTraining;
	public Transform scienceTraining;
	private Slider speedVial;
	private Slider fightingVial;
	private Slider scienceVial;

	public Transform hatchPoint;
	public Transform arenaPoint;
	public GameObject monsterPrefab;
	
	public GameObject buttonInfo;
	public GameObject releaseInfo;

	[SerializeField] private MonsterInstanceStats currentMonster;

	public TrainingRoomInput trainingRoomInput;
	private Cereal kelloggs = new Cereal();
	private Cereal vibrationKelloggs = new Cereal();
	
	private Stats.MonsterStatsGenes genePool = new Stats.MonsterStatsGenes();
	private int collectedGenes = 0;

	private AIVisibleObjects visibleObjects;
	private GameRoot gameRoot;

	private void Start()
	{
		gameRoot = FindObjectOfType<GameRoot> ();
		visibleObjects = FindObjectOfType<AIVisibleObjects> ();
		
		trainingRoomInput = GetComponent<TrainingRoomInput> ();

		// get vials
		speedVial = speedTraining.GetComponentInChildren<Slider> ();
		fightingVial = fightingTraining.GetComponentInChildren<Slider> ();
		scienceVial = scienceTraining.GetComponentInChildren<Slider> ();
		
		SetTrainingMode( currentTrainingMode );
	}

	private void Update()
	{
		kelloggs.Update ();
		vibrationKelloggs.Update ();

		UpdateStatVials ();
		
		buttonInfo.SetActive(null != currentMonster);
		releaseInfo.SetActive(null != currentMonster);
	}

	public bool HasMonster
	{
		get { return currentMonster != null; }
	}

	private void UpdateStatVials()
	{
		// set sum of genes and training
		if(currentMonster != null)
		{
			speedVial.value = Mathf.Clamp01 (currentMonster.Stats.Swoosh / MAX_STAT_VALUE);
			fightingVial.value = Mathf.Clamp01 (currentMonster.Stats.Strongth / MAX_STAT_VALUE);
			scienceVial.value = Mathf.Clamp01 (currentMonster.Stats.Science / MAX_STAT_VALUE);
		}
		// set only genes
		else
		{
			speedVial.value = Mathf.Clamp01 (genePool.swoosh / MAX_STAT_VALUE);
			fightingVial.value = Mathf.Clamp01 (genePool.strongth / MAX_STAT_VALUE);
			scienceVial.value = Mathf.Clamp01 (genePool.science / MAX_STAT_VALUE);
		}
	}

	public void CheatTrainSwoosh()
	{
		if (currentMonster == null)
			return;

		currentMonster.Stats.Training.TrainSwoosh (TRAINING_PER_CLICK);
	}

	public void CheatTrainStrongth()
	{
		if (currentMonster == null)
			return;
		
		currentMonster.Stats.Training.TrainStrongth (TRAINING_PER_CLICK);
	}

	public void CheatTrainScience()
	{
		if (currentMonster == null)
			return;

		currentMonster.Stats.Training.TrainScience (TRAINING_PER_CLICK);
	}

	public bool TrainStat ()
	{
		if (currentMonster == null)
			return false;

		switch(currentTrainingMode)
		{
		case TrainingMode.NONE:
			break;
		case TrainingMode.SPEED:
			currentMonster.Stats.Training.TrainSwoosh(TRAINING_PER_CLICK);
			break;
		case TrainingMode.FIGHTING:
			currentMonster.Stats.Training.TrainStrongth(TRAINING_PER_CLICK);
			break;
		case TrainingMode.SCIENCE:
			currentMonster.Stats.Training.TrainScience(TRAINING_PER_CLICK);
			break;
		}
		
		TrainAnimator anim = GetAnimator( currentTrainingMode );
		if( anim )
		{
			anim.Train();
		}

		return true;
	}
	
	private Transform GetTrainingTransform( TrainingMode mode )
	{
		switch(mode)
		{
		case TrainingMode.NONE:
			return null;
		case TrainingMode.SPEED:
			return speedTraining;
		case TrainingMode.FIGHTING:
			return fightingTraining;
		case TrainingMode.SCIENCE:
			return scienceTraining;
		}
		
		return null;
		
	}
	
	private TrainAnimator GetAnimator( TrainingMode mode )
	{
		Transform t = GetTrainingTransform( mode );
		if( null != t)
		{
			return t.GetComponent<TrainAnimator>();
		}
		return null;
	}
	
	public void SetTrainingMode( TrainingMode mode )
	{
		TrainAnimator prevAnim = GetAnimator( currentTrainingMode );
		if( prevAnim )
		{
			prevAnim.StopTraining();
		}

		currentTrainingMode = mode;
		
		
		TrainAnimator newAnim = GetAnimator( currentTrainingMode );
		if( newAnim )
		{
			newAnim.Train();
		}
		
		Transform trainTrans = GetTrainingTransform( currentTrainingMode );
		
		if( null != trainTrans )
		{
			buttonInfo.transform.position = trainTrans.position;
		}
		
	}

	public void NextTrainingMode ()
	{
		if( null == currentMonster )
			return;
	
		switch(currentTrainingMode)
		{
		case TrainingMode.NONE:
			SetTrainingMode( TrainingMode.SPEED );
			break;
		case TrainingMode.SPEED:
			SetTrainingMode( TrainingMode.FIGHTING );
			break;
		case TrainingMode.FIGHTING:
			SetTrainingMode( TrainingMode.SCIENCE );
			break;
		case TrainingMode.SCIENCE:
			SetTrainingMode( TrainingMode.SPEED );
			break; 
		}
	}

	public void PreviousTrainingMode ()
	{
		if( null == currentMonster )
			return;
			
		switch(currentTrainingMode)
		{
		case TrainingMode.NONE:
			SetTrainingMode( TrainingMode.SCIENCE );
			break;
		case TrainingMode.SPEED:
			SetTrainingMode( TrainingMode.SCIENCE );
			break;
		case TrainingMode.FIGHTING:
			SetTrainingMode( TrainingMode.SPEED );
			break;
		case TrainingMode.SCIENCE:
			SetTrainingMode( TrainingMode.FIGHTING );
			break;
		}
	}
	
	private PlayerIndex GetPlayerIndexFromGamepadID (int ID)
	{
		switch(ID)
		{
		case 0:
			return PlayerIndex.One;
		case 1:
			return PlayerIndex.Two;
		case 2:
			return PlayerIndex.Three;
		case 3:
			return PlayerIndex.Four;
		default:
			return PlayerIndex.One;
		}
	}

	private void VibrateController(float duration)
	{
		PlayerIndex controllerID = GetPlayerIndexFromGamepadID(trainingRoomInput.GetGamepad().ID);
		Debug.LogWarning ("Vibration on controller " + controllerID.ToString());
		GamePad.SetVibration (controllerID, VIBRATION_POWER, VIBRATION_POWER);
		
		vibrationKelloggs.Clear ();
		vibrationKelloggs.AddDelay (duration);
		vibrationKelloggs.AddAction (() =>
		                    {
			GamePad.SetVibration (controllerID, 0f, 0f);
		});
	}

	public void HatchMonster()
	{
		if (currentMonster != null)
			return;
		
		// vibration
		VibrateController (0.5f);

		GameObject monster = Instantiate(monsterPrefab, hatchPoint.position, Quaternion.identity) as GameObject;
		monster.transform.SetParent (gameRoot.transform, false);

		currentMonster = monster.GetComponent<MonsterInstanceStats> ();
		
		currentMonster.OnHatch( this, genePool, trainingRoomInput.playerColor );
		
		Master master = monster.GetComponentInChildren<Master> ();
		master.index = GetComponent<Gamepad>().ID - 1;
		
		//Turn on animation that was turned off when last monster left
		TrainAnimator anim = GetAnimator( currentTrainingMode );
		if( anim )
		{
			anim.Train();
		}

		// start invisible
		HideMonster ();
	}

	private void HideMonster()
	{
		if(currentMonster == null)
			return;

		currentMonster.transform.FindChild ("Gfx").gameObject.SetActive(false);
		currentMonster.transform.FindChild ("Healthbar").gameObject.SetActive(false);
		//currentMonster.transform.FindChild ("Gfx").GetComponent<SpriteRenderer> ().enabled = false;
	}

	private void ShowMonster()
	{
		if(currentMonster == null)
			return;
		
		currentMonster.transform.FindChild ("Gfx").gameObject.SetActive(true);
		currentMonster.transform.FindChild ("Healthbar").gameObject.SetActive(true);
		//currentMonster.transform.FindChild ("Gfx").GetComponent<SpriteRenderer> ().enabled = true;
	}

	public void ReleaseAllMonsters()
	{
		Door door = GetComponent<Door> ();
		if (door != null && !door.IsClosed)
			return;

		foreach(TrainingRoom tr in FindObjectsOfType<TrainingRoom>())
		{
			tr.OpenDoor((trainingRoom) => trainingRoom.MoveMonsterToArena(),	// onOpened
			             (trainingRoom) => trainingRoom.HatchMonster());	// onClosed
		}
	}

	public void OpenDoor (Action<TrainingRoom> OnOpened, Action<TrainingRoom> OnClosed)
	{
		if (currentMonster == null)
			return;

		kelloggs.Clear ();
		Door door = GetComponent<Door> ();
		if(door != null)
		{
			door.Open();
			kelloggs.WaitUntil(() => door.IsOpen);
			kelloggs.AddAction(() => {
				if(OnOpened != null)
				{
					OnOpened.Invoke(this);
				}
			});

			// close doors after all monsters are released
			kelloggs.AddDelay(1.5f);
			kelloggs.AddAction(() => {
				door.Close();
			});

			// when doors are closed, spawn new monsters
			if(OnClosed != null)
			{
				kelloggs.WaitUntil(() => door.IsClosed);
				kelloggs.AddAction(() => {
					OnClosed.Invoke (this);
				});
			}
		}
	}

	public void RemoveMonsterFromVisibleObject(MonsterInstanceStats monster)
	{
		visibleObjects.RemoveObject (monster.GetComponentInChildren<Master>().gameObject);
	}

	public void MoveMonsterToArena()
	{
		if (currentMonster == null)
			return;

		currentMonster.OnRelease ();
		currentMonster.transform.position = arenaPoint.position;

		// add to visible objects
		visibleObjects.AddObject (currentMonster.GetComponentInChildren<Master>().gameObject);

		// show monster
		ShowMonster ();

		currentMonster = null;
		
		//Turn off animation
		TrainAnimator anim = GetAnimator( currentTrainingMode );
		if( anim )
		{
			anim.StopTraining();
		}
	}

	private void OnDrawGizmos()
	{
		if (hatchPoint != null) {
			Gizmos.color = Color.green;
			Gizmos.DrawSphere (hatchPoint.position, 0.2f);
		}

		if (arenaPoint != null) {
			Gizmos.color = Color.magenta;
			Gizmos.DrawSphere (arenaPoint.position, 0.2f);
		}

		// current training mode
		Gizmos.color = Color.yellow;
		switch (currentTrainingMode) {
		case TrainingMode.NONE:
			Gizmos.DrawSphere (hatchPoint.position, 0.5f);
			break;
		case TrainingMode.SPEED:
			Gizmos.DrawSphere (speedTraining.position, 0.5f);
			break;
		case TrainingMode.FIGHTING:
			Gizmos.DrawSphere (fightingTraining.position, 0.5f);
			break;
		case TrainingMode.SCIENCE:
			Gizmos.DrawSphere (scienceTraining.position, 0.5f);
			break;
		}
	}

	public void PickUpGene( Stats.MonsterStatsGenes genes )
	{
		genePool.Add( genes );
		collectedGenes++;
	}

	public int CollectedGenes {
		get {
			return collectedGenes;
		}
	}

}
