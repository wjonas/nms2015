﻿using UnityEngine;
using System.Collections;

public class TrainAnimator : MonoBehaviour {

	const float VOLUME_HACK = 0.5f;

	public Sprite idleSprite;
	public Sprite[] trainingSprites;
	
	private int trainingIndex = 0;

	private AudioSource _sound;

	private bool _training = false;
	
	
	void Start()
	{
		StopTraining();

		_sound = GetComponent<AudioSource> ();
		_sound.volume = VOLUME_HACK;
	}
	
	public void StopTraining()
	{
		trainingIndex = -1;
		GetComponent<SpriteRenderer>().sprite = idleSprite;

		_training = false;
	}
	
	public void Train()
	{
		trainingIndex++;
		trainingIndex = trainingIndex % trainingSprites.Length;
		
		GetComponent<SpriteRenderer>().sprite = trainingSprites[trainingIndex];

		if (_training)
		{
			_sound.Play ();
		}

		_training = true;
	}
	
}
