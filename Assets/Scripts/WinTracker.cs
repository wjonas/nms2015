﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class WinTracker : MonoBehaviour {

	public Text text;
	public ArenaSpawner geneSpawner;
	private VictoryPanel _panel;
	private Gamepad _gamepad;
	
	private bool gameOver = false;
	private ApplicationSession session;
	
	void Start()
	{
		_panel = GetComponentInChildren<VictoryPanel>();
		_panel.gameObject.SetActive( false );
		
		_gamepad = GetComponent<Gamepad>();
	}

	public void Setup (ApplicationSession session)
	{
		this.session = session;
	}
	
	private int GetGenesLeft()
	{
		return geneSpawner.maxSpawns - geneSpawner.Spawns + geneSpawner.GetAliveSpawnedObjectsCount();
	}
	
	// Update is called once per frame
	void Update () {
	
		if (session == null)
			return;

		if ( ! gameOver )
		{
			int left = GetGenesLeft();
			text.text = "x " + GetGenesLeft();
			
			if( left <= 0)
			{
				EndGame ();
			}
		}
		else
		{
			//if( _gamepad.IsButtonDown(Gamepad.START_BUTTON) )
			if(IsAnyStartButtonPressed())
			{
				session.gameOver = true;
				//Application.LoadLevel("SceneArena");
			}
		}
	}

	private bool IsAnyStartButtonPressed()
	{
		for(int i = 1; i <= 4; ++i)
		{
			if(Input.GetButtonDown (string.Format ("{0}_{1}", "Start", i)))
			{
				return true;
			}
		}
		return false;
	}
	
	private void EndGame()
	{
		if (! gameOver )
		{
			gameOver = true;
			
			TrainingRoom[] rooms = FindObjectsOfType<TrainingRoom>();
			
			List< TrainingRoom > winners = new List< TrainingRoom >();
			
			foreach( TrainingRoom r in rooms )
			{
				if( winners.Count == 0 || winners[0].CollectedGenes < r.CollectedGenes )
				{
					winners.Clear();
					winners.Add ( r );
				}
				else if( winners[0].CollectedGenes == r.CollectedGenes )
				{
					winners.Add ( r );
				}
			}
			
			string winText = "";
			for( int i = 0; i < winners.Count; ++i )
			{
				string playerName = "Player " + winners[i].gameObject.GetComponent<Gamepad>().ID;
				winText += playerName;
				if( i != winners.Count - 1)
				{
					winText += ", ";
				}
			}
			winText += " wins!";
			
			
			_panel.gameObject.SetActive( true );
			
			_panel.text.text = winText;
			
		}
	}
}