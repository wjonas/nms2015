﻿using UnityEngine;
using System.Collections;

public static class Utils {

	public static T GetComponentInParents<T>( this MonoBehaviour mb )
	{
		Transform t = mb.transform;
		while( t != null )
		{
			T compInP = t.GetComponentInParent<T>();
			if( compInP != null )
			{
				return compInP;
			}
			
			t = t.parent;
			
		}
		
		return default(T);
		
	}
	
	private static T GetComponentInAllChildrenHelper<T>( Transform p )
	{
		for( int i = 0; i < p.childCount; ++i )
		{
			Transform c = p.GetChild(i);
			
			T component = c.GetComponent<T>();
			
			if( null != component )
			{
				return component;
			}
			
			T recursiveComp = GetComponentInAllChildrenHelper<T>( c );
			if( null != recursiveComp )
			{
				return recursiveComp;
			}
			
		}
		
		return default(T);
	
	}
	
	public static T GetComponentInAllChildren<T>( this MonoBehaviour mb )
	{
		Transform t = mb.transform;
		return GetComponentInAllChildrenHelper<T>( t );
	}
}
