﻿using UnityEngine;
using System.Collections;

public class MonsterInstanceStats : MonoBehaviour {

	private bool punched = false;
	
	private float currentLifeTime = 0f;
	private float currentHealth = 0f;
	
	private Stats.MonsterStatsSum stats = new Stats.MonsterStatsSum();
	private Vector2 startScale;
	
	public TrainingRoom home;
	public GameObject deathSplatPrefab;
	private Healthbar healthbar;
	private Tinter tinter;
	private Brain brain;

	void Start()
	{
		brain = GetComponentInChildren<Brain> ();
		tinter = GetComponentsInChildren<Tinter>(true)[0];
		startScale = transform.localScale;
		healthbar = this.GetComponentInAllChildren<Healthbar> ();
		UpdateToStats();
	}
	
	void Update () {
		currentLifeTime += Time.deltaTime;
		
		StatCheck();

		UpdateToStats();

		UpdateHealthbar ();

		if (brain._superBehavior == Brain.SuperBehavior.ARENAING) {
			Damage (Time.deltaTime * 0.3f, false);
		}

		/*if(Input.GetKeyUp(KeyCode.Space))
		{
			VibrateController(0.5f);
		}*/
	}

	void UpdateHealthbar ()
	{
		//Debug.Log ("current Health: " + currentHealth);
		healthbar.value = currentHealth / stats.Health;
	}
	
	void StatCheck()
	{
		/*if( currentLifeTime > stats.LifeTime )
		{
			Die ();
		}*/
	}
	
	public void Damage( float damage, bool showHurt )
	{
		if( damage > 0f )
		{
			currentHealth -= damage;

			if(showHurt)
				tinter.Hurt();
			
			if( currentHealth <= 0f )
			{
				Die();
			}
		}
	}
	
	public void OnHatch( TrainingRoom home, Stats.MonsterStatsGenes genePool, Color color )
	{
		this.home = home;
		currentLifeTime = 0.0f;
		
		stats.SetGenes( genePool.Clone() as Stats.MonsterStatsGenes );
		
		GetComponentInChildren<Tinter>().SetBaseColor( color );
	}
	
	public void OnRelease(  )
	{	

		brain.SetSuperBehaviour(Brain.SuperBehavior.ARENAING);

		currentHealth = stats.Health;
	}
	
	public void UpdateToStats()
	{
		float scaleX = stats.Width * startScale.x;
		float scaleY = stats.Height * startScale.y;
		
		this.transform.localScale = new Vector3(scaleX, scaleY, 0);

		StatCheck();
	}

	void Die()
	{
		Object.Instantiate( deathSplatPrefab, this.transform.position, Quaternion.identity );
	
		// hatch monster when dead
		home.HatchMonster ();
		home.RemoveMonsterFromVisibleObject (this);

		Destroy(this.gameObject);
	}
	
	void FixedUpdate()
	{
		punched = false;
	}

	
	public void MarkPunched()
	{
		punched = true;
	}
	public bool IsPunched()
	{
		return punched;
	}

	public Stats.MonsterStatsSum Stats {
		get {
			return stats;
		}
	}

	public float CurrentHealth {
		get {
			return currentHealth;
		}
	}
}
