﻿using UnityEngine;
using System.Collections;

public class VisibleObject : MonoBehaviour
{
	protected AIVisibleObjects visibleObjects;
	
	protected void Start()
	{
		visibleObjects = FindObjectOfType<AIVisibleObjects> ();
		visibleObjects.AddObject (gameObject);
	}
	
	protected void OnDisable()
	{
		if( null != visibleObjects )
		{
			visibleObjects.RemoveObject (gameObject);
		}
		
	}
}