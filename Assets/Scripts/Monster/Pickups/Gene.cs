﻿using UnityEngine;
using System.Collections;

public class Gene : VisibleObject
{
	public Stats.MonsterStatsGenes genePacket = new Stats.MonsterStatsGenes();
	public Color geneColor = Color.white;

	private SpriteRenderer gfx;

	void Start()
	{
		base.Start();
		//genePacket.Randomize();
		
		//transform.localScale = transform.localScale * genePacket.SumGenes() / 5;

		gfx = GetComponentInChildren<SpriteRenderer> ();
		gfx.color = geneColor;
	}

}