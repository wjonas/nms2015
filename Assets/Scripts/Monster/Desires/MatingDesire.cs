﻿using UnityEngine;
using System.Collections;

public class MatingDesire : Desire {

	public float lust = 0.0f;
	
	private Vision _vision;
	private MonsterMove _move;
	
	public MatingDesire()
		:base( DesireType.MATING )
	{}
	
	void Start()
	{
		base.Start ();
		_vision = GetComponent<Brain>()._vision;
		_move = GetComponent<MonsterMove>();
	}
	
	void Update()
	{
		lust += _myStats.Stats.MateDesire * Time.deltaTime;
	}
	
	public override void Do()
	{
		GameObject monster = _vision.FindClosestInside<Master>( );
		if( null != monster )
		{
			_move.SetGoal( monster.transform.position );
		}
		
	}
	
	
	public override bool CanDo ()
	{
		return _vision.AnyInside<Master>( );
	}
	public override float GetImportance ()
	{
		return lust;
	}
	
	void OnCollisionStay2D( Collision2D other )
	{	
		Master master = other.gameObject.GetComponentInChildren<Master>();
		
		if( null != master  )
		{
			Debug.Log ("Kiss!");
		}
	}
}
