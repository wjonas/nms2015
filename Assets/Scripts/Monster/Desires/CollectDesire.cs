﻿using UnityEngine;
using System.Collections;

public class CollectDesire : Desire {

	private Vision _vision;
	private MonsterMove	_move;
	
	private float wantToCollect = 0;
	private Brain brain;

	public AudioSource goodSound;
	
	public CollectDesire()
		:base( DesireType.COLLECT )
	{}

	void Start()
	{
		
		base.Start ();
		_vision = GetComponent<Brain>()._vision;
		_move = this.GetComponent<MonsterMove>();
		brain = GetComponent<Brain>();
	}
	
	void Update()
	{
		wantToCollect += Time.deltaTime * _myStats.Stats.GeneDesire;
	}
	
	public override void Do()
	{
		GameObject gene = _vision.FindClosestInside<Gene>();
		if( null != gene )
		{
			_move.SetGoal( gene.transform.position );
			Debug.Log ("collect");
		}
	}


	public override bool CanDo ()
	{
		return _vision.AnyInside<Gene>();
	}

	public override float GetImportance ()
	{
		/*GameObject gene = _vision.FindClosestInside<Gene>();
		if( null != gene )
		{
			float distance = (this.transform.position - gene.transform.position).magnitude;
			float norm = distance / 100f;
			norm = 1.0f - norm;
			
			norm *= wantToCollect;
			return norm;
		}*/
		return wantToCollect;
	}
	
	void OnTriggerStay2D( Collider2D other )
	{
		Gene gene = other.gameObject.GetComponent<Gene>();
		
		if( null != gene )
		{
			if(this == brain.latestPicked)
			{
				wantToCollect *= 0.5f;
				_myStats.home.PickUpGene( gene.genePacket );
				Destroy( gene.gameObject );
				GetComponentInParent<Rigidbody2D>().velocity = Vector3.zero;

				goodSound.Play();
			}
		}
	}
}
