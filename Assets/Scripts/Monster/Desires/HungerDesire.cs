﻿using UnityEngine;
using System.Collections;

public class HungerDesire : Desire {

	public AudioSource eatSound;

	float hunger = 0;

	private Vision _vision;
	private MonsterMove	_move;
	private GameRoot gameRoot;

	public GameObject bajs;
	
	public HungerDesire()
		:base( DesireType.HUNGER )
	{}
	
	void Start()
	{
		base.Start ();
		
		gameRoot = FindObjectOfType<GameRoot> ();
		_vision = GetComponent<Brain>()._vision;
		_move = this.GetComponent<MonsterMove>();
	}
	
	public override void Do()
	{
		GameObject food = _vision.FindClosestInside<Food>();
		if( null != food )
		{
			_move.SetGoal( food.transform.position );
			//Debug.Log ("hunger");
		}
		
	}
	
	
	public override bool CanDo ()
	{
		return _vision.AnyInside<Food>();
	}
	public override float GetImportance ()
	{
		return hunger;
	}
	
	// Update is called once per frame
	void Update () {
		hunger += _myStats.Stats.EatDesire * Time.deltaTime;
		
	}
	
	void OnTriggerStay2D( Collider2D other )
	{
		Food food = other.gameObject.GetComponent<Food>();
		
		if( null != food )
		{
			hunger *= 0.5f;
			Destroy( food.gameObject );
			GameObject go = Object.Instantiate<GameObject>( bajs );
			go.transform.SetParent(gameRoot.transform, false);
			go.transform.position = this.transform.position;

			transform.GetComponentInParent<Rigidbody2D>().velocity *= 0.5f;

			eatSound.Play();
		}
	}
}
