﻿using UnityEngine;
using System.Collections;
using System;

public class FightDesire : Desire {

	public float fightingSpirit = 0.0f;

	private Vision _vision;
	private Master _master;
	private MonsterMove	_move;

	public float kickForce = 10;
	
	private Func<Master, bool> masterCheck;

	public AudioSource biteSound;
	
	public FightDesire()
		:base( DesireType.FIGHT )
	{}
	
	void Start()
	{
		
		base.Start ();
		_vision = GetComponent<Brain>()._vision;
		_move = this.GetComponent<MonsterMove>();
		_master = GetComponent<Master>();
		
		
		masterCheck = (master)  => { return master.index != _master.index ; };
	}
	
	void Update()
	{
		fightingSpirit += _myStats.Stats.FightDesire * Time.deltaTime;
	}
	
	public override void Do()
	{
		GameObject master = _vision.FindClosestInside<Master>( masterCheck );
		if( null != master )
		{
			_move.SetGoal( master.transform.position );
			
			
		}
		
	}
	
	
	public override bool CanDo ()
	{
		bool result = _vision.AnyInside<Master>( masterCheck );
		return result;
	}
	public override float GetImportance ()
	{
		return fightingSpirit;
	}
	
	void OnCollisionEnter2D( Collision2D other )
	{
		//Avoid punching both ways
		if( _myStats.IsPunched() )
		{
			return;
		}
	
		Master master = other.gameObject.GetComponentInChildren<Master>();
		
		if( null != master && masterCheck( master ) )
		{
			MonsterInstanceStats enemyStats = other.gameObject.GetComponent<MonsterInstanceStats>();			
			enemyStats.MarkPunched();
			_myStats.MarkPunched();
			
			//Combat roll
			float total = _myStats.Stats.FightSkill + enemyStats.Stats.FightSkill;
			float rand = UnityEngine.Random.Range(0.0f, total);
			
			
			GameObject loser = null;
			GameObject winner = null;
			
			if( rand  < _myStats.Stats.FightSkill )
			{
				//I win!
				loser = other.gameObject;
				winner = transform.parent.gameObject;
			}
			else
			{
				//I lose
				loser = transform.parent.gameObject;
				winner = other.gameObject;
			}
			
			//Screw up loser
			//Kick loser
			Vector2 kickDirection = loser.transform.position - winner.transform.position;
			kickDirection.Normalize();
			
			//Rotate the kick direction
			float randomRot = UnityEngine.Random.Range(-45.0f, 45.0f);
			kickDirection = Quaternion.AngleAxis(randomRot, Vector3.forward ) * kickDirection;
			
			MonsterInstanceStats	winnerStats = winner.GetComponent<MonsterInstanceStats>();
			loser.GetComponent<Rigidbody2D>().AddForce( kickDirection * 2f * winnerStats.Stats.FightSkill, ForceMode2D.Impulse );
			
			winner.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
			//Hurt loser
			loser.GetComponent<MonsterInstanceStats>().Damage( 1, true );
			
			//Decrease fighting spirit
			FightDesire otherDesire = other.gameObject.GetComponentInChildren<FightDesire>();
			otherDesire.fightingSpirit *= 0.5f;
			this.fightingSpirit *= 0.5f;

			biteSound.Play();
		}
	}
}
