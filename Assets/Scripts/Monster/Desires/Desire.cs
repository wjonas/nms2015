﻿using UnityEngine;
using System.Collections;

public abstract class Desire : MonoBehaviour {

	public enum DesireType { COLLECT, FIGHT, HUNGER, SURVIVE, MATING };
	
	private DesireType _type;

	protected MonsterInstanceStats	_myStats;
	
	public Desire( DesireType type )
	{
		_type = type;
	}
	
	protected void Start() 
	{
		_myStats = transform.GetComponentInParent<MonsterInstanceStats>();
	}

	public abstract float 	GetImportance();
	public abstract bool  	CanDo();
	public abstract void	Do();
	
	public DesireType Type {
		get {
			return _type;
		}
	}
}
