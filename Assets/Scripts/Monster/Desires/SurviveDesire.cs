﻿using UnityEngine;
using System.Collections;
using System;

public class SurviveDesire : Desire {

	private Vision _vision;
	private Master _master;
	private MonsterMove	_move;
	private Brain _brain;
	
	[SerializeField]private float debugImportance;
	
	
	private Func<Master, bool> masterCheck;
	
	public SurviveDesire()
		:base( DesireType.SURVIVE )
	{}
	
	void Start()
	{
		
		base.Start ();
		_vision = GetComponent<Brain>()._vision;
		_move = this.GetComponent<MonsterMove>();
		_master = GetComponent<Master>();
		_brain = GetComponent<Brain>();
		
		
		masterCheck = ( Master master)  => { return master.index != _master.index && master.GetComponent<Brain>().latestPicked != null &&  master.GetComponent<Brain>().latestPicked.Type == DesireType.FIGHT; };
	}
	
	
	public override void Do()
	{
		Debug.Log ("run away");
		GameObject enemy = _vision.FindClosestInside<Master>( masterCheck );
		if( null != enemy )
		{
			Vector2 away = transform.position - enemy.transform.position;
			away.Normalize();
			_move.SetDirection( away );
		}
		
	}
	
	
	public override bool CanDo ()
	{
		if ( _brain.latestPicked != null  && _brain.latestPicked.Type == DesireType.FIGHT )
		{
			return false;
		}
		return _vision.AnyInside<Master>( masterCheck );
	}
	public override float GetImportance ()
	{
		GameObject enemy = _vision.FindClosestInside<Master>( masterCheck );
		if( null != enemy )
		{
			float healthNorm = _myStats.CurrentHealth / _myStats.Stats.Health;
			healthNorm = 1f - healthNorm;
			
			float distance = (transform.position - enemy.transform.position).magnitude;
			float distFactor = Math.Max( 0.0f, 400 - distance );
			
			debugImportance = distFactor * healthNorm;
			
			return distFactor * healthNorm;
			
			
			
		}
		else
		{
			return 0;
		}
	}
	
	
}
