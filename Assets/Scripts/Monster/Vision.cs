﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Vision : MonoBehaviour
{

	//List< Collider2D > inside = new List< Collider2D >();
	CircleCollider2D _collider;

	private AIVisibleObjects visibleObjects;

	void Start()
	{
		visibleObjects = FindObjectOfType<AIVisibleObjects> ();
		_collider = GetComponent< CircleCollider2D >();
	}

	//If there is any object with component T inside vision
	public bool AnyInside<T>( Func<T, bool> onCheck = null )
	{
		foreach( GameObject go in visibleObjects.visibleObjects )
		{
			if( go != null )
			{
				T comp = go.GetComponent<T>();
				if( null != comp )
				{
					if( null == onCheck || onCheck.Invoke( comp ) )
					{
						return true;
					}
				}
			}
			
		}
		return false;
	}

	//Find object inside vision that has component T, that is closest to position
	public GameObject FindClosestInside<T>(Func<T, bool> onCheck = null)
	{
		float shortestDistance = -1;
		GameObject closest = null;
		foreach( GameObject go in visibleObjects.visibleObjects )
		{
			if( null == go )
			{
				continue;
			}
				
			T comp = go.GetComponent<T>();
			if( null != comp )
			{
				if( null == onCheck || onCheck.Invoke( comp ) )
				{
					Vector2 diff = go.transform.position - this.transform.position;
					float distance = diff.sqrMagnitude; //Square magnitude for performance!
					
					if(shortestDistance < 0 )
					{
						closest = go;
						shortestDistance = distance;
					}
					else
					{
						if( distance < shortestDistance )
						{
							closest = go;
							shortestDistance = distance;
						}
					}
				}
			}
		}
		
		return closest;
	}
}
