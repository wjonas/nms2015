﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public static class Stats {
	
	[Serializable]
	public class MonsterStatsCommon : ICloneable
	{
		public float science = 0.0f;
		public float strongth = 0.0f;
		public float swoosh = 0.0f;

		public object Clone()
		{
			return this.MemberwiseClone();
		} 
		
		public float Health {
			get {
				return 5f;
			}
		}
		
		public float GeneDesire {
			get {
				return science;
			}
		}
		
		public float FightDesire {
			get {
				return strongth;
			}
		}
		
		public float FightSkill {
			get {
				return strongth;
			}
		}
		
		public float EatDesire {
			get {
				return strongth + swoosh;
			}
		}

		public float Speed {
			get {
				return swoosh;
			}
		}
		
		public float LifeTime {
			get {
				return swoosh * 5;
			}
		}
		
		public float Science {
			get {
				return science;
			}
		}
		
		public float Swoosh {
			get {
				return swoosh;
			}
		}
		
		public float Strongth {
			get {
				return strongth;
			}
		}
	}
	
	[Serializable]
	public class MonsterStatsTraining : MonsterStatsCommon
	{
	
		//Training functions
		public void TrainScience( float change )
		{
			TrainInternal( ref science, change, -10.0f, 10.0f );
		}

		public void TrainStrongth( float change )
		{
			TrainInternal( ref strongth, change, -10.0f, 10.0f );
		}

		public void TrainSwoosh( float change )
		{
			TrainInternal( ref swoosh, change, -10.0f, 10.0f );
		}

		private void TrainInternal( ref float stat, float change, float min, float max )
		{
			stat += change;
			Mathf.Clamp( stat, min, max );
		}
				
	}
	
	[Serializable]
	public class MonsterStatsGenes : MonsterStatsCommon
	{
		public MonsterStatsGenes()
		{
			swoosh = 1.0f;
			science = 1.0f;
			strongth = 1.0f;
		}
		
		public void Add( MonsterStatsGenes genes )
		{
			swoosh += genes.swoosh;
			science += genes.science;
			strongth += genes.strongth;
		}
		
		public void Randomize()
		{
			swoosh = UnityEngine.Random.Range(0.1f, 1.0f);
			science = UnityEngine.Random.Range(0.1f, 1.0f);
			strongth = UnityEngine.Random.Range(0.1f, 1.0f);
		}
		
		public float SumGenes()
		{
			return swoosh + strongth + science;
		}
		
	}
	
	[Serializable]
	public class MonsterStatsSum
	{
		private MonsterStatsGenes genes = new MonsterStatsGenes();
		private MonsterStatsTraining training = new MonsterStatsTraining();
		
		public float LifeTime {
			get {
				return Mathf.Max( 1.0f, genes.LifeTime + training.LifeTime);
			}
		}
		
		public float Health {
			get {
				return Mathf.Max ( 1.0f, genes.Health + training.Health );
			}
		}
		
		public float GeneDesire
		{
			get {
				return Mathf.Max ( 0.0f, genes.GeneDesire + training.GeneDesire );
			}
		}
		
		public float FightDesire
		{
			get {
				return Mathf.Max ( 0.0f, genes.FightDesire + training.FightDesire);
			}
		}
		
		public float MateDesire
		{
			get {
				return 1;
			}
		}
		
		public float EatDesire
		{
			get {
				return Mathf.Max ( 0.0f, genes.EatDesire + training.EatDesire);
			}
		}

		
		public float Speed
		{
			get {

				return Mathf.Max ( 0.1f, genes.Speed + training.Speed);
			}
		}
		public float Science
		{
			get {
				
				return genes.Science + training.Science;
			}
		}
		public float Strongth
		{
			get {
				
				return genes.Strongth + training.Strongth;
			}
		}
		public float Swoosh
		{
			get {
				
				return genes.Swoosh + training.Swoosh;
			}
		}


		public float FightSkill {
			get {
				return Mathf.Max ( 0.1f, genes.FightSkill + training.FightSkill);
			}
		}

		public MonsterStatsTraining Training {
			get {
				return training;
			}
		}

		public void SetGenes ( MonsterStatsGenes genes )
		{
			this.genes = genes;
		}
		
		public float Height {
			get {
				return 1f + (genes.strongth + training.strongth) * 0.1f;
			}
		}
		
		public float Width {
			get {
				return Mathf.Max(0.3f, 1f + (genes.strongth + training.strongth) * 0.1f - (genes.swoosh + training.swoosh) * 0.1f);
			}
		}
	}
	
}
