﻿using UnityEngine;
using System.Collections;

public class Tinter : MonoBehaviour {

	public AnimationCurve blinkCurve;
	
	[SerializeField]private Color baseColor;
	public Color hurtColor;

	public float hurtTime = 4;
	public float currentTime;
	
	public bool hurt = false;
	
	private SpriteRenderer _renderer;
	
	void Start()
	{
		currentTime = hurtTime * 2;
		_renderer = GetComponent<SpriteRenderer>();
		
	}
	
	void Update() 
	{
		if( hurt )
		{
			hurt = false;
			Hurt ();
		}
	
		currentTime += Time.deltaTime;
		
		float hurtValue = 0.0f;
		
		if( currentTime < hurtTime )
		{
			hurtValue = blinkCurve.Evaluate( currentTime / hurtTime );
		}
		
		//Set according to hurtValue
		Color totalColor = Color.Lerp( baseColor, hurtColor, hurtValue );
		_renderer.color = totalColor;
		
	}


	public void Hurt()
	{
		currentTime = 0;
	}
	
	
	public void SetBaseColor( Color color )
	{
		baseColor = color;
	}
}
