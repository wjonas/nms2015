﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Brain : MonoBehaviour {

	public enum SuperBehavior { TRAINING, ARENAING };
	public SuperBehavior _superBehavior = SuperBehavior.TRAINING;
	
	List< Desire > desires = new List< Desire >();
	public Vision _vision;

	public Desire latestPicked;

	public void SetSuperBehaviour(SuperBehavior superBehavior)
	{
		_superBehavior = superBehavior;
	}

	private void Start () {
		desires = new List<Desire>( GetComponents<Desire>() );
	}

	private void Update () {
	
		if( SuperBehavior.ARENAING == _superBehavior )
		{
			Desire highestDesire = null;
			float highestImportance = -1;
			foreach( Desire d in desires )
			{
				if( d.CanDo() )
				{
					float imp = d.GetImportance();
					if( highestImportance < 0 || highestImportance < imp )
					{
						highestImportance = imp;
						highestDesire = d;
					}
				}
			}
			
			if( null != highestDesire )
			{
				latestPicked = highestDesire;
				highestDesire.Do();
			}
			else
			{
				GetComponent< MonsterMove>().Stop();
			}
		}
		else
		{
			GetComponent< MonsterMove>().Stop();
		}
	}
}
