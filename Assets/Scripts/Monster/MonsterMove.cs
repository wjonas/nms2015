﻿using UnityEngine;
using System.Collections;

public class MonsterMove : MonoBehaviour {

	enum MoveMode { TO_GOAL, DIRECTION, STOPPED }
	
	MoveMode moveMode = MoveMode.STOPPED;

	private Vector2 direction;
	private Vector2 goal;
	private Vector2 lastDir;
	private Rigidbody2D _rigidbody;
	private MonsterInstanceStats	_myStats;
	private Animator monsterAnimator;
	private Brain brain;

	// Use this for initialization
	void Start () 
	{
		brain = GetComponent<Brain> ();
		monsterAnimator = transform.parent.Find ("Gfx").GetComponent<Animator> ();

		_myStats = transform.parent.GetComponent<MonsterInstanceStats>();
		_rigidbody = transform.parent.GetComponent<Rigidbody2D>();
	}
	
	void AddDirectionForce(	Vector2 dir )
	{
		lastDir = dir;
		_rigidbody.AddForce( dir * _myStats.Stats.Speed );
	}

	void Update()
	{
		if (!monsterAnimator.gameObject.activeSelf || brain == null)
			return;

		// idling to walking
		if(_rigidbody.velocity.magnitude > 0.1f)
		{
			if(monsterAnimator.enabled)
			{
				if(brain.latestPicked is FightDesire)
				{
					if(!monsterAnimator.GetBool("Hunting"))
					{
						monsterAnimator.SetBool("Walking", false);
						monsterAnimator.SetBool("Hunting", true);
						monsterAnimator.SetBool("Fleeing", false);
					}
				}
				else if(brain.latestPicked is SurviveDesire)
				{
					if(!monsterAnimator.GetBool("Fleeing"))
					{
						monsterAnimator.SetBool("Walking", false);
						monsterAnimator.SetBool("Hunting", false);
						monsterAnimator.SetBool("Fleeing", true);
					}
				}
				else
				{
					if(!monsterAnimator.GetBool("Walking"))
					{
						monsterAnimator.SetBool("Walking", true);
						monsterAnimator.SetBool("Hunting", false);
						monsterAnimator.SetBool("Fleeing", false);
					}
				}
			}
		}
		// walking to idling
		if(_rigidbody.velocity.magnitude == 0f)
		{
			if(monsterAnimator.enabled && monsterAnimator.GetBool("Walking"))
				monsterAnimator.SetBool("Walking", false);
		}

		if(_rigidbody.velocity.x < 0.05f)
		{
			if(monsterAnimator.enabled && monsterAnimator.transform.localScale.x >= 0f)
			{
				Vector3 newScale = monsterAnimator.transform.localScale;
				newScale.x *= -1f;
				monsterAnimator.transform.localScale = newScale;
			}
		}

		if(_rigidbody.velocity.x > 0.05f)
		{
			if(monsterAnimator.enabled && monsterAnimator.transform.localScale.x < 0f)
			{
				Vector3 newScale = monsterAnimator.transform.localScale;
				newScale.x *= -1f;
				monsterAnimator.transform.localScale = newScale;
			}
		}
	}

	void FixedUpdate()
	{
		//_Rigidbody.AddForce( Force, ForceMode2D.Force );
		if( moveMode == MoveMode.DIRECTION )
		{
			AddDirectionForce( direction );
		}
		if( moveMode == MoveMode.TO_GOAL )
		{
			Vector2 diff = goal - (Vector2)this.transform.position;
			
			if( diff.magnitude > 0 )
			{
				if( Vector2.Dot( diff, lastDir ) < 0 )
				{
					//Past the goal
					Stop ();
				}
				else
				{
					diff.Normalize();
					AddDirectionForce( diff );
				}
			}
			else
			{
				Debug.LogError("AAA! Goal is on us!");
			}
		}
		
	}
	
	public void SetGoal( Vector2 newGoal )
	{
		this.goal = newGoal;
		moveMode = MoveMode.TO_GOAL;
		
	}
	
	public void SetDirection( Vector2 newDir )
	{
		direction = newDir;
		
		if (direction.sqrMagnitude > 0 )
		{
			direction.Normalize();
		}
		moveMode = MoveMode.DIRECTION;
		
	}
	
	public void Stop()
	{
		moveMode = MoveMode.STOPPED;
		lastDir = Vector2.zero;
	}
}
