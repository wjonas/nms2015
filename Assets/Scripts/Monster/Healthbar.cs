﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Healthbar : MonoBehaviour {

	[Range(0f, 1f)]public float value = 1f;

	private SpriteRenderer background;
	private SpriteRenderer foreground;

	void Start () {
		background = transform.FindChild ("Background").GetComponent<SpriteRenderer>();
		foreground = transform.FindChild ("Foreground").GetComponent<SpriteRenderer>();
	}

	void Update()
	{
		SetValue (value);
	}

	private void SetValue(float value)
	{
		Vector3 newScale = foreground.transform.localScale;
		newScale.y = Mathf.Clamp01(value);
		foreground.transform.localScale = newScale;
		foreground.transform.localPosition = new Vector3 (-1f + newScale.y, 0f, 0f);
	}
}