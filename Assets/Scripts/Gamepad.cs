﻿using UnityEngine;
using System.Collections;

public class Gamepad : MonoBehaviour  {

	[Range(1,4)]public int ID = 1;	// user ID 1-4

	public const string A_BUTTON = "A";
	public const string B_BUTTON = "B";
	public const string X_BUTTON = "X";
	public const string Y_BUTTON = "Y";
	public const string START_BUTTON = "Start";
	public const string BACK_BUTTON = "Back";
	
	public const string DPAD_HORIZONTAL_AXIS = "DPad_XAxis";
	public const string DPAD_VERTICAL_AXIS = "DPad_YAxis";
	public const string L_X_AXIS = "L_XAxis";
	public const string L_Y_AXIS = "L_YAxis";

	public bool IsButtonDown(string buttonName)
	{
		if(Input.GetButtonDown (string.Format ("{0}_{1}", buttonName, ID)))
		{
			//Debug.Log (string.Format ("Pressed {0}_{1}", buttonName, ID));
			return true;
		}
		return false;
	}
	
	public bool IsButton(string buttonName)
	{
		string str = string.Format ("{0}_{1}", buttonName, ID);
		if(Input.GetButton(str))
		{
			return true;
		}
		return false;
	}
	
	public float UpdateAxis(string axisName)
	{
		// debug if not zero
		float value = Input.GetAxis (string.Format ("{0}_{1}", axisName, ID));
		if(value != 0f)
		{
			//Debug.Log (string.Format ("Axis {0}_{1}: {2}", axisName, ID, value));
		}
		return value;
	}


}
