﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class ArenaSpawner : MonoBehaviour
{
	[SerializeField] private Rect spawningArea;
	[SerializeField] private float minTime;
	[SerializeField] private float maxTime;
	[SerializeField] private GameObject[] pickups;

	private Transform itemParent;
	private GameRoot gameRoot;

	[SerializeField] private int spawns = 0;
	[SerializeField] private int scheduledSpawns = 0;
	public int maxSpawns = -1;
	
	private List<GameObject> spawnedObjects = new List<GameObject>();

	private void Start()
	{
		gameRoot = FindObjectOfType<GameRoot> ();
		CreateItemParent ();
		SpawnLater();
	}
	
	private void SpawnLater()
	{
		if( scheduledSpawns < maxSpawns || maxSpawns < 0 )
		{
			Invoke("SpawnRandomPickup", Random.Range(minTime, maxTime));
			scheduledSpawns++;
		}
		
	}
	
	private bool FinishedSpawning()
	{
		return spawns >= maxSpawns;
	}
	
	public int GetAliveSpawnedObjectsCount()
	{
		spawnedObjects.RemoveAll ( (item) => { return item == null; });
		return spawnedObjects.Count;
	}

	private void CreateItemParent ()
	{
		GameObject go = new GameObject ();
		go.transform.SetParent (gameRoot.transform, false);
		go.name = string.Format ("_ItemParent [{0}]", 0);
		itemParent = go.transform;
	}

	private void SpawnRandomPickup()
	{
		//Debug.Log ("SPAWN PICKUP");

		int rnd = Random.Range (0, pickups.Length);
		Vector2 randomSpawnPoint = new Vector2 (
			Random.Range (spawningArea.xMin, spawningArea.xMax),
			Random.Range (spawningArea.yMin, spawningArea.yMax));
		GameObject go = GameObject.Instantiate (pickups [rnd], 
		                                        (Vector2)transform.position + randomSpawnPoint, Quaternion.Euler(0f, 0f, Random.Range(0f, 360f))) as GameObject;

		go.transform.SetParent (itemParent, false);
		spawnedObjects.Add ( go );
		
		itemParent.name = string.Format ("_ItemParent [{0}]", itemParent.childCount);
		
		spawns++;

		SpawnLater();
	}

	private void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		Gizmos.matrix = transform.localToWorldMatrix;
		Gizmos.DrawWireCube (spawningArea.center, spawningArea.size);
	}

	public int Spawns {
		get {
			return spawns;
		}
	}
}