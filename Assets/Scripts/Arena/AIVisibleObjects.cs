﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AIVisibleObjects : MonoBehaviour {

	public List<GameObject> visibleObjects = new List<GameObject>();

	public void AddObject(GameObject go)
	{
		visibleObjects.Add(go);
	}
	
	public void RemoveObject(GameObject go)
	{
		visibleObjects.Remove(go);
	}
}