using UnityEngine;
using System.Collections;
using System.Text;

public class ApplicationRunner : MonoBehaviour
{
	public static ApplicationRunner instance;
	public ApplicationSession session;

	public bool isShowingDebugLog;

	private void Awake()
	{
		if(instance)
		{
			DestroyImmediate(gameObject);
			return;
		}
		else
		{
			DontDestroyOnLoad(gameObject);
			instance = this;

			Application.targetFrameRate = 60;

			// start the application session and go to boot state
			session = new ApplicationSession(this);
			session.GotoState(new StateBoot());
		}
	}

	private void Update()
	{
		if(session.isPaused)
			return;

		session.UpdateLoadingState();

		if(session.state != null)
		{
			session.state.UpdateState();
		}
	}

	/*private void OnGUI()
	{
		// state
		if (session != null && session.state != null) {
			GUILayout.BeginArea (new Rect (Screen.width - 400 - 5, 35, 400, 400));
			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.Label (session.state.ToString ());
			GUILayout.EndHorizontal ();
			GUILayout.EndArea ();
		}
	}*/
}