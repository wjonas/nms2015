﻿using UnityEngine;
using System.Collections;

public class AutoLoader : MonoBehaviour
{
	ApplicationRunner runner;

	void Awake()
	{
		AutoLoader otherLoader = FindObjectOfType<AutoLoader>();
		if(otherLoader != null && otherLoader != this)
		{
			Destroy(gameObject);
			return;
		}

		runner = FindObjectOfType<ApplicationRunner>();
		if(runner == null)
		{
			Application.LoadLevel(0);
		}
		
		Destroy(gameObject);
	}
}