using UnityEngine;
using System.Collections;

public class StateGameSetup : StateBase
{
	private AsyncOperation loadOp;

	public override void Begin(ApplicationSession pSession)
	{
		base.Begin(pSession);

		SetupGame();

		// Start loading the level asynchronously
		loadOp = Application.LoadLevelAdditiveAsync(session.gameScene);
	}

	public override void UpdateState()
	{
		if(loadOp != null && loadOp.isDone)
		{
			session.GotoState(new StateGamePlay());
		}
	}

	private void SetupGame()
	{
	}

	public override void End()
	{
		base.End();
	}
}