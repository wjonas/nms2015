using UnityEngine;
using System.Collections;
using Ebbe.Helpers;

public class StateGamePlay : StateBase
{
	public override bool IsLoadingComplete(ApplicationSession pSession)
	{
		return pSession != null && pSession.gameRoot != null;
	}

	public override void Begin(ApplicationSession pSession)
	{
		base.Begin(pSession);

		// hatch monsters in each base
		foreach(TrainingRoom tr in GameObject.FindObjectsOfType<TrainingRoom>())
		{
			tr.HatchMonster();
		}

		// setup game
		if (session.gameRoot != null)
		{
			session.gameRoot.Setup();
		}
	}

	public override void UpdateState()
	{
		if (session.gameOver) {
			session.gameOver = false;
			RestartGame();
		}

		if(Input.GetKeyDown(KeyCode.Escape))
		{
			Application.Quit();
		}

		/*if(Input.anyKey && !session.hasStarted)
		{
			session.gameRoot.splashScreen.SetActive(false);
			session.hasStarted = true;
		}*/

		// escape shows pause menu
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			GotoPauseMenu();
		}
		if(Input.GetKeyDown(KeyCode.Return))
		{
			RestartGame();
		}
	}

	private void RestartGame()
	{
		//session.hasStarted = false;
		session.GotoState(new StateGameTeardown(new StateGameSetup()));
	}

	private void GotoPauseMenu()
	{
		session.GotoState(new StateGamePaused(this));
	}

	public override void End()
	{
		base.End();
	}
}