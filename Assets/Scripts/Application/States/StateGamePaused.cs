﻿using UnityEngine;
using System.Collections;

public class StateGamePaused : StateBase
{
	private StateBase returnState;

	public StateGamePaused(StateBase pReturnState)
	{
		returnState = pReturnState;
	}

	public override void Begin(ApplicationSession pSession)
	{
		base.Begin(pSession);
		
		// pause timer
		session.Pause();
	}

	public override void UpdateState()
	{
		// escape go to start menu
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			RestartGame();
		}
	}

	private void RestartGame()
	{
		session.GotoState (new StateGameTeardown ());
	}

	private void ResumeGame()
	{
		if(returnState != null)
		{
			session.GotoState(returnState);
		}
		else
		{
			Debug.LogError("Has no game state to return to.");
		}
	}

	public override void End()
	{
		// resume timer
		session.Resume();

		base.End();
	}
}