﻿using UnityEngine;
using System.Collections;

public class StateGameTeardown : StateBase
{
	private StateBase nextState;

	public StateGameTeardown() {}
	public StateGameTeardown(StateBase pNextState)
	{
		nextState = pNextState;
	}

	public override void Begin(ApplicationSession pSession)
	{
		base.Begin(pSession);

		DestroyGame();

		// if we have a next state to go to
		if(nextState != null)
		{
			session.GotoState(nextState);
		}
	}

	private void DestroyGame()
	{
		// Destroy the game root
		if(session.gameRoot != null)
		{
			GameObject.Destroy(session.gameRoot.gameObject);
			session.gameRoot = null;
		}
	}

	public override void End()
	{
		base.End();
	}
}