﻿using UnityEngine;
using System.Collections;
using Ebbe.Cereal;

public class StateBoot : StateBase
{
	private Cereal _kelloggs = new Cereal();

	public override void Begin(ApplicationSession pSession)
	{
		base.Begin(pSession);

		// load GUI scene first, then go to gameplay state
		Application.LoadLevelAdditive(session.loadLevel);

		_kelloggs.AddAction(() =>
		{
			session.GotoState(new StateGameSetup());
		});
	}

	public override void UpdateState()
	{
		_kelloggs.Update();
	}

	public override void End()
	{
		base.End();
	}
}