using UnityEngine;
using System.Collections;

public class StateGameOver : StateBase
{
	public override bool IsLoadingComplete(ApplicationSession pSession)
	{
		return pSession != null && pSession.gameRoot != null;
	}

	public override void Begin(ApplicationSession pSession)
	{
		base.Begin(pSession);

		session.GotoState(new StateGameTeardown(new StateGamePlay()));
	}

	public override void UpdateState()
	{
	}

	public override void End()
	{
		base.End();
	}
}