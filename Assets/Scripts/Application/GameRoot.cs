using UnityEngine;
using System.Collections;
using XInputDotNetPure;

public class GameRoot : MonoBehaviour
{
	private ApplicationSession session;
	private WinTracker winTracker;
	//public GameObject splashScreen;

	private void Start()
	{
		ApplicationRunner runner = GameObject.FindObjectOfType<ApplicationRunner>();
		if(runner != null)
		{
			session = runner.session;
			if(session != null)
			{
				session.gameRoot = this;
			}
		}
	}

	public void Setup()
	{
		winTracker = FindObjectOfType<WinTracker> ();
		winTracker.Setup (session);

		//splashScreen = GameObject.Find ("Splash");
	}

	private void OnDisable()
	{
		// emergency turn off vibration
		GamePad.SetVibration (PlayerIndex.One, 0f, 0f);
		GamePad.SetVibration (PlayerIndex.Two, 0f, 0f);
		GamePad.SetVibration (PlayerIndex.Three, 0f, 0f);
		GamePad.SetVibration (PlayerIndex.Four, 0f, 0f);
	}

}