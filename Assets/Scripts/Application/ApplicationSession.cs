using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ApplicationSession
{
	public StateBase state;
	public ApplicationRunner runner;
	public GameRoot gameRoot;

	public string loadLevel = "SceneBoot";
	public string gameScene = "SceneArena";

	public bool isPaused = false;
	public bool hasStarted = false;
	public bool gameOver = false;

	private StateBase loadingState;

	public ApplicationSession(ApplicationRunner pRunner)
	{
		runner = pRunner;
	}

	public bool UpdateLoadingState()
	{
		if(loadingState != null && loadingState.IsLoadingComplete(this))
		{
			state = loadingState;
			state.Begin(this);
			loadingState = null;
			return true;
		}
		return false;
	}

	public StateBase GotoState(StateBase pState)
	{
		//Debug.Log(string.Format("Goto state {0} >> {1}.", (state == null ? "\"no state\"" : state.ToString()), pState.ToString()));
		if(state != null)
		{
			state.End();
		}

		if(pState.IsLoadingComplete(this))
		{
			state = pState;
			if(!pState.hasRunBegin)
			{
				state.Begin(this);
			}
		}
		else
		{
			loadingState = pState;
			state = null;
			UpdateLoadingState();
		}

		return pState;
	}

	public void Pause()
	{
		isPaused = true;
	}

	public void Resume()
	{
		isPaused = false;
	}
}
