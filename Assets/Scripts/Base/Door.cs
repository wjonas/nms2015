﻿using UnityEngine;
using System.Collections;
using Ebbe.Cereal;

public class Door : MonoBehaviour
{
	[SerializeField] private AnimationCurve doorAnimationCurve;
	[SerializeField] private float openDuration = 1f;
	[SerializeField] private Transform door;
	[SerializeField] private Transform targetOpenDoor;
	[SerializeField] private ParticleSystem closedDustParticles;

	[SerializeField] private AudioClip _soundOpen;
	[SerializeField] private AudioClip _soundClose;

	private AudioSource _audioSource;

	private Cereal kelloggs = new Cereal();
	private float value = 0f;	// how open the door is
	private Vector3 startPosition;

	public bool IsOpen
	{
		get { return value >= 1f; }
	}
	
	public bool IsClosed
	{
		get { return value <= 0f; }
	}

	private void Start()
	{
		startPosition = door.position;

		_audioSource = GetComponent<AudioSource> ();
	}

	private void Update()
	{
		kelloggs.Update ();
		if(Input.GetKeyDown(KeyCode.O))
		{
			Open ();
		}
		if(Input.GetKeyDown(KeyCode.C))
		{
			Close ();
		}
	}

	public void Open()
	{
		if (value == 1f)
			return;

		kelloggs.Clear ();
		kelloggs.Add(new CerealLerpEvent<float>(value, 1f, openDuration * (1f-value), Mathf.Lerp, (newValue) => {
			UpdateValue(newValue);
		}));

		_audioSource.PlayOneShot (_soundOpen);
	}

	public void Close()
	{
		if (value == 0f)
			return;

		kelloggs.Clear ();
		kelloggs.Add(new CerealLerpEvent<float>(value, 0f, openDuration * value, Mathf.Lerp, (newValue) => {
			UpdateValue(newValue);
		}));

		_audioSource.PlayOneShot (_soundClose);
	}

	private void UpdateValue (float newValue)
	{
		if (newValue < value && newValue == 0f && closedDustParticles != null) 
		{
			closedDustParticles.Play();
		}

		value = newValue;
		door.position = Vector3.Lerp (startPosition, targetOpenDoor.position, doorAnimationCurve.Evaluate(newValue));
	}

	private void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawSphere (targetOpenDoor.position, 0.2f);
		Gizmos.DrawLine (transform.position, targetOpenDoor.position);
	}
}