﻿using UnityEngine;
using System.Collections;

public class GamepadSymbol : MonoBehaviour {

	private SpriteRenderer _renderer;
	public Sprite upSprite;
	public Sprite downSprite;
	
	public string button;
	
	private Gamepad _gamepad;

	void Start()
	{
		_gamepad = this.GetComponentInParents<Gamepad>();
		_renderer = GetComponent<SpriteRenderer>();
		
		SetPressed( false );
	}
	
	void Update()
	{
		SetPressed( _gamepad.IsButton(button) );
	}


	void SetPressed( bool pressed )
	{
		if( pressed )
		{
			_renderer.sprite = downSprite;
		}
		else
		{
			_renderer.sprite = upSprite;
		}
		
	}
}
