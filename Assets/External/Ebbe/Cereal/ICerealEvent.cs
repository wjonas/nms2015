using System;
using System.Collections;

namespace Ebbe.Cereal
{
	public interface ICerealEvent
	{
		void Begin();
		void Update();
		void End();
		bool CheckComplete();
	}
}
