using System;
using System.Collections;
using UnityEngine;

namespace Ebbe.Cereal
{
	public class CerealDelayEvent : CerealEvent
	{
		private double _duration;
		private double _startTime;

		public CerealDelayEvent(double duration)
		{
			_duration = duration;
			_startTime = timeProvider.time;

			OnCheckComplete = () =>
			{
				return timeProvider.time >= _startTime + _duration;
			};
		}

		public override string ToString()
		{
			return string.Format("{0}, {1:00.00} s duration", this.GetType().Name, _duration);
		}
	}
}
