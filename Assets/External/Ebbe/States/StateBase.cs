﻿using UnityEngine;

public class StateBase : IBase
{
	public ApplicationSession session;
	public bool hasRunBegin;

	public virtual bool IsLoadingComplete(ApplicationSession pSession)
	{
		return true;
	}

	public virtual void Begin(ApplicationSession pSession)
	{
		session = pSession;
		hasRunBegin = true;
	}

	public virtual void UpdateState()
	{
	}

	public virtual void End()
	{
	}

	public override string ToString()
	{
		return this.GetType().ToString();
	}
}