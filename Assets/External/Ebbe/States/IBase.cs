﻿public interface IBase
{
	void Begin(ApplicationSession pSession);

	void UpdateState();

	void End();
}