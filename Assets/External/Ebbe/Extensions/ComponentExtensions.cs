﻿using UnityEngine;
using System.Collections;

namespace Ebbe.Helpers
{
	public static class ComponentExtensions
	{
		public static T GetOrAddComponent<T>(this GameObject go) where T : Component
		{
			T comp = go.GetComponent<T>();
			if(comp == null)
			{
				comp = go.AddComponent<T>();
			}
			return comp;
		}
	}
}