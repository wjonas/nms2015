﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.CodeDom.Compiler;

namespace Ebbe.Helpers
{
	public static class StringExtensions
	{
		public static string[] FastSplit(this string data, string[] delimiters)
		{
			// return empty string
			if(data.Length == 0)
			{
				return new string[] { string.Empty };
			}

			// TODO: sort delimiters so that longest are first

			List<string> splits = new List<string>();
			List<char> c = new List<char>();
			bool skip = false;
			int delimiterLength;
			for (int ci = 0; ci < data.Length; ++ci)
			{
				// compare to delimiters
				for (int i = 0; i < delimiters.Length; ++i)
				{
					delimiterLength = delimiters[i].Length;
					if (data.Length >= ci + delimiterLength + 1 && delimiters[i].Equals(data.Substring(ci, delimiterLength)))
					{
						ci += delimiterLength - 1;

						// found delimiter, add previous word
						splits.Add(new string(c.ToArray()));
						c.Clear();
						skip = true;
						break;
					}
				}

				if (skip)
				{
					skip = false;
				}
				else
				{
					// found character, add to word
					c.Add(data[ci]);
				}
			}

			splits.Add(new string(c.ToArray()));

			Debug.Log(string.Format("Found {0} words.", splits.Count));

			return splits.ToArray();
		}
	}
}