﻿using UnityEngine;
using System.Collections;
using System.Text;

namespace Ebbe.Helpers
{
	public static class CharArrayExtensions
	{
		public static string CharArrayToString(this char[] array)
		{
			if(array == null || array.Length == 0)
				return "[]";

			StringBuilder sb = new StringBuilder();
			for(int i = 0; i < array.Length; ++i)
			{
				if(i < array.Length - 1)
					sb.Append(array[i] + ", ");
				else
					sb.Append(array[i]);
			}
			return string.Format("[{0}]", sb.ToString());
		}
	}
}