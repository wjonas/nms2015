﻿using UnityEngine;
using System.Collections;
using System.Text;

namespace Ebbe.Helpers
{
	public static class TransformExtensions
	{
		public static string FullName(this Transform pSelf)
		{
			StringBuilder sb = new StringBuilder();
			while(pSelf != null)
			{
				sb.Insert(0, pSelf.name);
				pSelf = pSelf.parent;
				if(pSelf != null)
					sb.Insert(0, "/");
			}
			return sb.ToString();
		}

		public static Transform FindRecursive(this Transform pSelf, string pName)
		{
			Transform trans;
			for(int i = 0; i < pSelf.childCount; ++i)
			{
				trans = pSelf.GetChild(i);
				Transform result = FindRecursiveInternal(trans, pName);
				if(result != null)
				{
					return result;
				}
			}

			Debug.LogError(string.Format("Could not find Transform {0} inside {1}.", pName, pSelf.FullName()));
			return null;
		}

		private static Transform FindRecursiveInternal(Transform trans, string pName)
		{
			if(trans.name == pName)
			{
				return trans;
			}
			Transform child;
			for(int i = 0; i < trans.childCount; ++i)
			{
				child = trans.GetChild(i);
				Transform result = FindRecursiveInternal(child, pName);
				if(result != null)
				{
					return result;
				}
			}
			return null;
		}
	}
}