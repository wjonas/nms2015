﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;

/// Author: Edvard Rutström <edvard.rutstrom@gmail.se>
/// 
/// Copyright (c) 2014 
/// 
/// This library is free software; you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as
//  published by the Free Software Foundation; either version 2.1 of the
//  License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful, but
//  WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

/// <summary> 
/// A generic pool for objects that inherit from UnityEngine.MonoBehaviour and has a parameterless contstructor new().
/// </summary>
/// <typeparam name="T">Type of the pooled objects.</typeparam>
public class PoolMonoBehaviour<T> : IDisposable where T : MonoBehaviour, new()
{
	const int START_CAPACITY = 100;

	private Action<T> onCleanUp;

	private List<T> used;
	private List<T> free;

	private Transform parent;
	private GameObject prefab;

	//public PoolMonoBehaviour() : this(START_CAPACITY, null) { }

	/// <summary>
	/// Callback method called when returning pooled object to pool. Send in for no clean up.
	/// </summary>
	/// <param name="pOnCleanUp">Callback method.</param>
	public PoolMonoBehaviour(int pStartCapacity, GameObject pObjectPrefab, Action<T> pOnCleanUp = null)
	{
		used = new List<T>(pStartCapacity);
		free = new List<T>(pStartCapacity);

		prefab = pObjectPrefab;

		onCleanUp = pOnCleanUp;

		Initialize(pStartCapacity);
	}

	private void Initialize(int pStartCapacity)
	{
		for(int i = 0; i < pStartCapacity; ++i)
		{
			free.Add(CreateObject());
		}

		parent.name = GetPoolNameWithObjectCount();
	}

	public T FetchObject()
	{
		// expand array with new objects if we have no more free objects to return
		if(free.Count == 0)
		{
			T toUse = CreateObject();
			used.Add(toUse);
			parent.name = GetPoolNameWithObjectCount();
			toUse.gameObject.SetActive(true);
			return toUse;
		}
		else
		{
			T toUse = free[0];
			free.RemoveAt(0);
			used.Add(toUse);
			parent.name = GetPoolNameWithObjectCount();
			toUse.gameObject.SetActive(true);
			return toUse;
		}
	}

	private T CreateObject()
	{
		GameObject go = GameObject.Instantiate(prefab, Vector3.zero, Quaternion.identity) as GameObject;
		go.transform.parent = GetParent();
		go.name = GetCreatedObjectName();
		T t = GetOrAddComponent(go);
		go.SetActive(false);
		return t;
	}

	private T GetOrAddComponent(GameObject go)
	{
		T t = go.GetComponent<T>();
		if(t == null)
			t = go.AddComponent<T>();

		return t;
	}

	private string GetCreatedObjectName()
	{
		return string.Format("{0}_{1}", prefab.name, TotalObjectsCount());
	}

	private string GetPoolNameWithObjectCount()
	{
		return string.Format("{0}Pool ({1}/{2})", prefab.name, used.Count, TotalObjectsCount());
	}

	private int TotalObjectsCount()
	{
		return free.Count + used.Count;
	}

	private Transform GetParent()
	{
		if(parent == null)
		{
			GameObject go = new GameObject();
			go.name = string.Format("Object pool");
			parent = go.transform;
		}
		return parent;
	}

	public void ReturnObject(T pObject)
	{
		CleanUp(pObject);

		pObject.gameObject.SetActive(false);

		used.Remove(pObject);
		free.Add(pObject);

		parent.name = GetPoolNameWithObjectCount();
	}

	private void CleanUp(T pObject)
	{
		if(onCleanUp != null)
		{
			onCleanUp.Invoke(pObject);
		}
	}

	public int CountFreeObjects()
	{
		return free.Count;
	}

	public int CountUsedObjects()
	{
		return used.Count;
	}

	public int CountTotalObjects()
	{
		return free.Count + used.Count;
	}

	public void Dispose()
	{
		free.Clear();
		used.Clear();
		free = null;
		used = null;

		// TODO: Dispose of all instanced objects in the pool

		parent.name = GetPoolNameWithObjectCount();
	}
}