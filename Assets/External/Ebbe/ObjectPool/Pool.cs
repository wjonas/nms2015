﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;

/// Author: Edvard Rutström <edvard.rutstrom@gmail.se>
/// 
/// Copyright (c) 2014 
/// 
/// This library is free software; you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as
//  published by the Free Software Foundation; either version 2.1 of the
//  License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful, but
//  WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

/// <summary> 
/// A generic pool for objects that inherit from UnityEngine.Object and has a parameterless contstructor new().
/// </summary>
/// <typeparam name="T">Type of the pooled objects.</typeparam>
public class Pool<T> : IDisposable where T : UnityEngine.Object, new()
{
	const int START_CAPACITY = 100;

	private Action<T> onCleanUp;

	private List<T> used;
	private List<T> free;

	public Pool() : this(START_CAPACITY, null) { }

	/// <summary>
	/// Callback method called when returning pooled object to pool. Send in for no clean up.
	/// </summary>
	/// <param name="pOnCleanUp">Callback method.</param>
	public Pool(int pStartCapacity, Action<T> pOnCleanUp = null)
	{
		used = new List<T>(pStartCapacity);
		free = new List<T>(pStartCapacity);

		onCleanUp = pOnCleanUp;
	}

	public T GetObject()
	{
		// expand array with new objects if we have no more free objects to return
		if(free.Count == 0)
		{
			T toUse = new T();
			used.Add(toUse);
			return toUse;
		}
		else
		{
			T toUse = free[0];
			free.RemoveAt(0);
			used.Add(toUse);
			return toUse;
		}
	}

	public void ReleaseObject(T pObject)
	{
		CleanUp(pObject);

		used.Remove(pObject);
		free.Add(pObject);
	}

	private void CleanUp(T pObject)
	{
		if(onCleanUp != null)
		{
			onCleanUp.Invoke(pObject);
		}
	}

	public int CountFreeObjects()
	{
		return free.Count;
	}

	public int CountUsedObjects()
	{
		return used.Count;
	}

	public int CountTotalObjects()
	{
		return free.Count + used.Count;
	}

	public void Dispose()
	{
		free.Clear();
		used.Clear();
		free = null;
		used = null;
	}
}